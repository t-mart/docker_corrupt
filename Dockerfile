FROM maven:latest
VOLUME /root/.m2

COPY . /root/app

WORKDIR /root/app

CMD mvn install
