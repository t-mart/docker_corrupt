#!/usr/bin/env bash


rm -rvf "myvol/*" || mkdir -pv myvol
docker build -t dockercorrupt . && docker run -v $(pwd)/myvol:/root/.m2 dockercorrupt

echo "Docker build and run complete"

curuser=$(whoami)
curgroups=$(groups)

shopt -s globstar
for f in myvol/**/*; do
    u=$(stat --format="%U" "$f")
    g=$(stat --format="%G" "$f")

    echo -n "$f "
    [[ $u == $curuser ]] && echo -en "\e[32m${u}\e[39m " || (echo -en "\e[31m${u}\e[39m " && quit)
    [[ $curgroups =~ .*"g".* ]] && echo -e "\e[32m${g}\e[39m" || (echo -e "\e[31m${g}\e[39m" && quit)
done